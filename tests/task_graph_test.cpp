#include "scheduling/task_graph.h"

#include <gtest/gtest.h>

namespace {
using namespace testing;

TEST(TaskGroupTest, CalculateSumNoCallback) {
  constexpr auto a = 42;
  constexpr auto b = 53;
  std::atomic sum = 0;

  const scheduling::ThreadPool thread_pool(std::thread::hardware_concurrency());

  scheduling::TaskGraph task_graph(thread_pool);

  const auto get_a = task_graph.AddTask([] { return a; });
  const auto get_b = task_graph.AddTask([] { return b; });

  task_graph
      .AddTask<int, int>(
          [](const auto lhs, const auto rhs) { return lhs + rhs; },
          [&sum](const auto result) {
            sum = result;
            sum.notify_one();
          })
      .WithArgs(get_a, get_b);

  task_graph.Submit();

  sum.wait(0);
  EXPECT_EQ(sum, a + b);
}

TEST(TaskGroupTest, CalculateSumWithCallback) {
  constexpr auto a = 42;
  constexpr auto b = 53;
  std::atomic sum = 0;

  const scheduling::ThreadPool thread_pool(std::thread::hardware_concurrency());

  scheduling::TaskGraph task_graph(thread_pool);

  const auto get_a = task_graph.AddTask([] { return a; });
  const auto get_b = task_graph.AddTask([] { return b; });

  task_graph
      .AddTask<int, int>([&sum](const auto lhs, const auto rhs) {
        sum = lhs + rhs;
        sum.notify_one();
      })
      .WithArgs(get_a, get_b);

  task_graph.Submit();

  sum.wait(0);
  EXPECT_EQ(sum, a + b);
}

TEST(TaskGroupTest, CalculateExpression) {
  constexpr auto a = 42;
  constexpr auto b = 53;
  constexpr auto c = 68;
  constexpr auto d = 94;
  std::atomic result = 0;

  const scheduling::ThreadPool thread_pool(std::thread::hardware_concurrency());

  scheduling::TaskGraph task_graph(thread_pool);

  const auto get_a = task_graph.AddTask([] { return a; });
  const auto get_b = task_graph.AddTask([] { return b; });
  const auto get_c = task_graph.AddTask([] { return c; });
  const auto get_d = task_graph.AddTask([] { return d; });

  const auto calculate_sum = [](const auto lhs, const auto rhs) {
    return lhs + rhs;
  };

  const auto get_sum_1 =
      task_graph.AddTask<int, int>(calculate_sum).WithArgs(get_a, get_b);

  const auto get_sum_2 =
      task_graph.AddTask<int, int>(calculate_sum).WithArgs(get_c, get_d);

  auto calculate_product = [](const auto lhs, const auto rhs) {
    return lhs * rhs;
  };

  auto result_callback = [&result](const auto value) {
    result = value;
    result.notify_one();
  };

  task_graph
      .AddTask<int, int>(std::move(calculate_product),
                         std::move(result_callback))
      .WithArgs(get_sum_1, get_sum_2);

  task_graph.Submit();

  result.wait(0);
  EXPECT_EQ(result, (a + b) * (c + d));
}

TEST(TaskGroupTest, CommonRoot) {
  constexpr auto a = 42;
  std::atomic doubled = 0;
  std::atomic squared = 0;

  const scheduling::ThreadPool thread_pool(std::thread::hardware_concurrency());

  scheduling::TaskGraph task_graph(thread_pool);

  const auto get_a = task_graph.AddTask([] { return a; });

  task_graph
      .AddTask<int>([](const auto a) { return 2 * a; },
                    [&](const auto result) {
                      doubled = result;
                      doubled.notify_one();
                    })
      .WithArgs(get_a);

  task_graph
      .AddTask<int>([](const auto a) { return a * a; },
                    [&](const auto result) {
                      squared = result;
                      squared.notify_one();
                    })
      .WithArgs(get_a);

  task_graph.Submit();

  doubled.wait(0);
  EXPECT_EQ(doubled, 2 * a);

  squared.wait(0);
  EXPECT_EQ(squared, a * a);
}
}  // namespace
