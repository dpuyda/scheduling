#include <iostream>

#include "scheduling/task_graph.h"
#include "scheduling/thread_pool.h"

int main() {
  const auto max_threads_count = std::thread::hardware_concurrency();

  scheduling::ThreadPool thread_pool(max_threads_count);

  // Submit a function without parameters that returns `void`:
  {
    auto task = [] { std::this_thread::sleep_for(std::chrono::seconds(1)); };

    auto callback = [] { std::cout << "Completed!" << std::endl; };

    thread_pool.Submit(std::move(task), std::move(callback));
  }

  // Submit a function without parameters that returns `int`:
  {
    auto task = [] { return 42; };

    auto callback = [](const int result) { std::cout << result << std::endl; };

    thread_pool.Submit(std::move(task), std::move(callback));
  }

  // Submit a function that calculates the sum of two integers:
  {
    auto task = [](const int a, const int b) { return a + b; };

    auto callback = [](const int sum) { std::cout << sum << std::endl; };

    thread_pool.Submit(std::move(task), std::move(callback), 42, 53);
  }

  // Submit a function with a given priority:
  {
    constexpr auto priority = scheduling::kDefaultPriority + 1;

    auto task = [] { return 42; };

    auto callback = [](const int result) { std::cout << result << std::endl; };

    thread_pool.Submit(priority, std::move(task), std::move(callback));
  }

  // Cancel a task if it has not started yet:
  {
    auto task = [] {
      std::this_thread::sleep_for(std::chrono::seconds(1));
      return 42;
    };

    auto callback = [](const int result) { std::cout << result << std::endl; };

    const auto cancellation_token =
        thread_pool.Submit(std::move(task), std::move(callback));

    if (const auto is_cancelled = cancellation_token.Cancel(); is_cancelled) {
      std::cout << "The task is cancelled successfully!" << std::endl;
    } else {
      std::cout << "The task cannot be cancelled" << std::endl;
    }
  }

  // A task graph to calculate `a + b`
  {
    scheduling::TaskGraph task_graph(thread_pool);

    const auto get_a = task_graph.AddTask([] {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      return 42;
    });

    const auto get_b = task_graph.AddTask([] {
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
      return 53;
    });

    auto calculate_sum = [](const auto a, const auto b) {
      std::this_thread::sleep_for(std::chrono::milliseconds(300));
      return a + b;
    };

    auto sum_callback = [](const auto sum) { std::cout << sum << std::endl; };

    [[maybe_unused]] const auto get_result =
        task_graph
            .AddTask<int, int>(std::move(calculate_sum),
                               std::move(sum_callback))
            .WithArgs(get_a, get_b);

    task_graph.Submit();
  }

  // A task graph to calculate `(a + b) * (c + d)`.
  {
    scheduling::TaskGraph task_graph(thread_pool);

    const auto get_a = task_graph.AddTask([] {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      return 42;
    });

    const auto get_b = task_graph.AddTask([] {
      std::this_thread::sleep_for(std::chrono::milliseconds(125));
      return 53;
    });

    const auto get_c = task_graph.AddTask([] {
      std::this_thread::sleep_for(std::chrono::milliseconds(150));
      return 68;
    });

    const auto get_d = task_graph.AddTask([] {
      std::this_thread::sleep_for(std::chrono::milliseconds(175));
      return 94;
    });

    const auto calculate_sum = [](const auto lhs, const auto rhs) {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      return lhs + rhs;
    };

    const auto get_sum_1 =
        task_graph.AddTask<int, int>(calculate_sum).WithArgs(get_a, get_b);

    const auto get_sum_2 =
        task_graph.AddTask<int, int>(calculate_sum).WithArgs(get_c, get_d);

    auto calculate_product = [](const auto lhs, const auto rhs) {
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
      return lhs * rhs;
    };

    auto product_callback = [](const auto product) {
      std::cout << product << std::endl;
    };

    [[maybe_unused]] const auto get_result =
        task_graph
            .AddTask<int, int>(std::move(calculate_product),
                               std::move(product_callback))
            .WithArgs(get_sum_1, get_sum_2);

    task_graph.Submit();
  }

  return 0;
}
