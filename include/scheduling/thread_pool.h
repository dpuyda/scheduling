#pragma once

#include <algorithm>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <numeric>
#include <thread>
#include <vector>

namespace scheduling {
namespace internal {
class ThreadPoolImpl;
}  // namespace internal

/**
 * The default task priority. The task queue is sorted by task priority.
 */
constexpr int kDefaultPriority = 0;

/**
 * Defines on what threads a submitted task can be executed.
 */
enum class Thread {
  /**
   * A submitted task can be executed on any thread. If the number of threads
   * running by the thread pool is smaller than the maximum number of threads
   * that can be run simultaneously, a new thread will be created to execute a
   * submitted task.
   *
   * @note If a new thread is created, there is no guarantee that the task will
   * be executed on the new thread. If a running thread becomes available sooner
   * than the new thread starts, the running thread can steal the task from
   * the new thread.
   */
  kAny,

  /**
   * A submitted task must be executed on the same thread where it is submitted.
   * The task must be submitted on a thread that is running by the thread pool.
   * Consider using this option when tasks submit other tasks to the same thread
   * pool instance.
   */
  kSame
};

/**
 * Allows to cancel a task that was submitted to a thread pool.
 */
class CancellationToken {
 public:
  CancellationToken(const CancellationToken&) = default;
  CancellationToken(CancellationToken&&) = default;
  CancellationToken& operator=(const CancellationToken&) = default;
  CancellationToken& operator=(CancellationToken&&) = default;
  ~CancellationToken() = default;

  /**
   * Attempts to cancel the submitted task. A task can be cancelled only if it
   * has not started yet.
   *
   * @return `true` if the task is cancelled successfully or `false` if the task
   * is already completed, is being executed or if the thread pool is destroyed.
   */
  [[nodiscard]] bool Cancel() const { return cancel_function_(); }

 private:
  friend class internal::ThreadPoolImpl;

  explicit CancellationToken(std::function<bool()> cancel_function)
      : cancel_function_{std::move(cancel_function)} {}

  std::function<bool()> cancel_function_;
};

namespace internal {
struct PrioritizedTask {
  PrioritizedTask(std::function<void()> task, const int priority)
      : task{std::move(task)}, priority{priority} {}
  std::function<void()> task;
  int priority;
};

struct Continuation {
  Continuation(std::function<void()> task, const std::thread::id thread_id)
      : task{std::move(task)}, thread_id{thread_id} {}
  std::function<void()> task;
  std::thread::id thread_id;
};

class ThreadPoolImpl : public std::enable_shared_from_this<ThreadPoolImpl> {
 public:
  explicit ThreadPoolImpl(const size_t max_threads_count) : disposing_{false} {
    task_threads_.resize(max_threads_count);
    available_threads_.resize(max_threads_count);
    std::iota(available_threads_.rbegin(), available_threads_.rend(), 0);
  }

  ThreadPoolImpl(const ThreadPoolImpl&) = delete;
  ThreadPoolImpl(ThreadPoolImpl&&) = delete;
  ThreadPoolImpl& operator=(const ThreadPoolImpl&) = delete;
  ThreadPoolImpl& operator=(ThreadPoolImpl&&) = delete;

  ~ThreadPoolImpl() {
    {
      std::lock_guard lock(mutex_);
      disposing_ = true;
    }

    for (auto& thread : task_threads_) {
      if (thread) {
        thread->join();
      }
    }
  }

  template <typename TaskType, typename CallbackType, typename... Args>
  CancellationToken Submit(const int priority, const Thread thread,
                           TaskType&& task, CallbackType&& callback,
                           Args&&... args) {
    auto execute = [this, task = std::forward<TaskType>(task),
                    callback = std::forward<CallbackType>(callback),
                    ... args = std::forward<Args>(args)]() mutable {
      using ResultType = std::invoke_result_t<TaskType, Args...>;
      if constexpr (std::is_void_v<ResultType>) {
        task(std::forward<Args>(args)...);
        callback();
      } else {
        auto result = task(std::forward<Args>(args)...);
        callback(std::move(result));
      }
    };

    if (task_threads_.empty()) {
      execute();
      return CancellationToken([] { return false; });
    }

    std::lock_guard lock(mutex_);

    if (disposing_) {
      return CancellationToken([] { return false; });
    }

    return thread == Thread::kSame
               ? AddContinuation(std::move(execute))
               : AddPrioritizedTask(std::move(execute), priority);
  }

 private:
  std::function<void()> GetTask() {
    std::function<void()> task = nullptr;
    if (const auto continuation_it =
            continuations_.find(std::this_thread::get_id());
        continuation_it != continuations_.end()) {
      task = std::move(continuation_it->second->task);
      continuations_.erase(continuation_it);
    } else {
      if (!prioritized_tasks_.empty()) {
        task = std::move(prioritized_tasks_.front()->task);
        prioritized_tasks_.pop_front();
      }
    }
    return task;
  }

  void CreateThread(const size_t thread_index) {
    auto run = [this](const size_t current_thread_index) {
      while (true) {
        std::unique_lock lock(mutex_);
        const auto task = GetTask();
        if (!task) {
          available_threads_.push_back(current_thread_index);
          return;
        }
        lock.unlock();
        task();
      }
    };

    auto& task_thread = task_threads_[thread_index];

    if (task_thread) {
      task_thread->join();
    }

    task_thread = std::make_unique<std::thread>(
        [run = std::move(run), thread_index] { run(thread_index); });
  }

  CancellationToken AddPrioritizedTask(std::function<void()> task,
                                       const int priority) {
    auto prioritized_task =
        std::make_shared<PrioritizedTask>(std::move(task), priority);

    const auto it =
        std::ranges::upper_bound(prioritized_tasks_, prioritized_task,
                                 [](const auto& lhs, const auto& rhs) {
                                   return lhs->priority > rhs->priority;
                                 });

    prioritized_tasks_.insert(it, prioritized_task);

    if (!available_threads_.empty()) {
      const auto thread_index = available_threads_.back();
      available_threads_.pop_back();
      CreateThread(thread_index);
    }

    return CancellationToken{[weak_thread_pool = weak_from_this(),
                              task = std::move(prioritized_task)] {
      if (const auto thread_pool = weak_thread_pool.lock()) {
        return thread_pool->Cancel(task);
      }
      return false;
    }};
  }

  CancellationToken AddContinuation(std::function<void()> task) {
    auto continuation = std::make_shared<Continuation>(
        std::move(task), std::this_thread::get_id());

    continuations_.emplace(std::this_thread::get_id(), continuation);

    return CancellationToken{[weak_thread_pool = weak_from_this(),
                              continuation = std::move(continuation)] {
      if (const auto thread_pool = weak_thread_pool.lock()) {
        return thread_pool->Cancel(continuation);
      }
      return false;
    }};
  }

  bool Cancel(const std::shared_ptr<PrioritizedTask>& prioritized_task) {
    std::lock_guard lock(mutex_);
    return std::erase(prioritized_tasks_, prioritized_task);
  }

  bool Cancel(const std::shared_ptr<Continuation>& continuation) {
    std::lock_guard lock(mutex_);
    return std::erase_if(continuations_, [&](const auto& item) {
      const auto& [key, value] = item;
      return value == continuation;
    });
  }

  std::mutex mutex_;
  std::list<std::shared_ptr<PrioritizedTask>> prioritized_tasks_;
  std::multimap<std::thread::id, std::shared_ptr<Continuation>> continuations_;
  std::vector<std::unique_ptr<std::thread>> task_threads_;
  std::vector<size_t> available_threads_;
  bool disposing_;
};
}  // namespace internal

/**
 * Allows to submit a task for execution to a thread pool. The number of threads
 * running simultaneously by the thread pool is limited. Copy-assigned and
 * copy-created `ThreadPool` instances share the same thread pool.
 */
class ThreadPool {
 public:
  /**
   * Creates a `ThreadPool` instance.
   *
   * @param max_threads_count The maximum number of threads that can be run
   * simultaneously by the thread pool.
   */
  explicit ThreadPool(const size_t max_threads_count)
      : impl_{std::make_shared<internal::ThreadPoolImpl>(max_threads_count)} {}

  ThreadPool(const ThreadPool&) = default;
  ThreadPool(ThreadPool&&) = default;
  ThreadPool& operator=(const ThreadPool&) = default;
  ThreadPool& operator=(ThreadPool&&) = default;
  ~ThreadPool() = default;

  /**
   * Submits a task with the default priority. Tasks submitted sooner are closer
   * to the beginning of the task queue than tasks submitted later. When
   * the task is completed, the callback is called. The task is executed and
   * the callback is called on a thread created by `ThreadPool`. The task will
   * be started when a thread for this task becomes available.
   *
   * @param task The task to submit.
   * @param args The task parameters.
   * @param callback The callback that will be called when the task is
   * completed.
   *
   * @return The cancellation token allowing to cancel the task.
   */
  template <typename TaskType, typename CallbackType, typename... Args>
  CancellationToken Submit(TaskType&& task, CallbackType&& callback,
                           Args&&... args) {
    return impl_->Submit(
        kDefaultPriority, Thread::kAny, std::forward<TaskType>(task),
        std::forward<CallbackType>(callback), std::forward<Args>(args)...);
  }

  /**
   * Submits a task with a given priority. When the task is completed,
   * the callback is called. The task is executed and the callback is called on
   * a thread created by `ThreadPool`. The task will be started when a thread
   * for this task becomes available.
   *
   * @param priority The task priority. Tasks with a higher priority are closer
   * to the beginning of the task queue than tasks with a lower priority. If
   * there are multiple tasks with the same priority, tasks submitted sooner are
   * closer to the beginning of the task queue than tasks submitted later. The
   * default task priority is 0.
   * @param task The task to submit.
   * @param args The task parameters.
   * @param callback The callback that will be called when the task is
   * completed.
   *
   * @return The cancellation token allowing to cancel the task.
   */
  template <typename TaskType, typename CallbackType, typename... Args>
  CancellationToken Submit(const int priority, TaskType&& task,
                           CallbackType&& callback, Args&&... args) {
    return impl_->Submit(priority, Thread::kAny, std::forward<TaskType>(task),
                         std::forward<CallbackType>(callback),
                         std::forward<Args>(args)...);
  }

  /**
   * Submits a task with the default priority. Tasks submitted sooner are closer
   * to the beginning of the task queue than tasks submitted later. When
   * the task is completed, the callback is called. The task is executed and
   * the callback is called on a thread created by `ThreadPool`. The task will
   * be started when a thread for this task becomes available.
   *
   * @param thread Defines on what threads the task can be executed.
   * @param task The task to submit.
   * @param args The task parameters.
   * @param callback The callback that will be called when the task is
   * completed.
   *
   * @return The cancellation token allowing to cancel the task.
   */
  template <typename TaskType, typename CallbackType, typename... Args>
  CancellationToken Submit(const Thread thread, TaskType&& task,
                           CallbackType&& callback, Args&&... args) {
    return impl_->Submit(kDefaultPriority, thread, std::forward<TaskType>(task),
                         std::forward<CallbackType>(callback),
                         std::forward<Args>(args)...);
  }

 private:
  std::shared_ptr<internal::ThreadPoolImpl> impl_;
};
}  // namespace scheduling
