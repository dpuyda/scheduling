#pragma once

#include <atomic>
#include <cassert>
#include <tuple>
#include <vector>

#include "thread_pool.h"

namespace scheduling {
namespace internal {
class ITask {
 public:
  ITask() = default;
  ITask(const ITask&) = default;
  ITask(ITask&&) = default;
  ITask& operator=(const ITask&) = default;
  ITask& operator=(ITask&&) = default;
  virtual ~ITask() = default;
  virtual void Wait() = 0;
};

template <typename ResultType>
class Result {
 public:
  void Store(ResultType&& result) {
    result_ = std::forward<ResultType>(result);
  }

  const ResultType& operator()() const { return result_.value(); }
  explicit operator bool() const { return result_.has_value(); }

 private:
  std::optional<ResultType> result_;
};

template <>
class Result<void> {};

template <typename TaskType, typename CallbackType, typename... ArgTypes>
class TaskImpl final : public ITask,
                       public std::enable_shared_from_this<
                           TaskImpl<TaskType, CallbackType, ArgTypes...>> {
 public:
  using ResultType = std::invoke_result_t<TaskType, ArgTypes...>;

  TaskImpl(TaskType&& task, CallbackType&& callback, ThreadPool thread_pool)
      : task_{std::forward<TaskType>(task)},
        callback_{std::forward<CallbackType>(callback)},
        thread_pool_{std::move(thread_pool)},
        pending_args_count_{sizeof...(ArgTypes)},
        result_{} {}

  TaskImpl(const TaskImpl&) = default;
  TaskImpl(TaskImpl&&) = default;
  TaskImpl& operator=(const TaskImpl&) = default;
  TaskImpl& operator=(TaskImpl&&) = default;
  ~TaskImpl() override = default;

  template <typename... ArgumentTasks>
  void WithArgs(ArgumentTasks&&... argument_tasks) {
    [this]<typename Tuple, size_t... I>(Tuple&& tuple,
                                        std::index_sequence<I...>) {
      (WithArg<I>(std::get<I>(tuple)), ...);
    }(std::forward_as_tuple(argument_tasks...),
      std::make_index_sequence<sizeof...(ArgTypes)>{});
  }

  void Submit(const Thread thread) {
    auto weak_this = std::enable_shared_from_this<
        TaskImpl<TaskType, CallbackType, ArgTypes...>>::weak_from_this();

    auto task = [weak_this] {
      if (const auto shared_this = weak_this.lock()) {
        if constexpr (std::is_same_v<ResultType, void>) {
          std::apply(shared_this->task_, std::move(shared_this->args_));
        } else {
          shared_this->result_.Store(
              std::apply(shared_this->task_, std::move(shared_this->args_)));
        }
      }
    };

    auto callback = [weak_this] {
      if (const auto shared_this = weak_this.lock()) {
        if constexpr (!std::is_same_v<CallbackType, nullptr_t>) {
          if constexpr (std::is_same_v<ResultType, void>) {
            shared_this->callback_();
          } else {
            shared_this->callback_(shared_this->result_());
          }
        }

        auto successor_thread = Thread::kSame;

        for (const auto& link : shared_this->links_) {
          link(successor_thread);
        }

        shared_this->is_completed_.test_and_set();
        shared_this->is_completed_.notify_one();
      }
    };

    thread_pool_.Submit(thread, std::move(task), std::move(callback));
  }

  void Wait() override { is_completed_.wait(false); }

 private:
  template <typename OtherTaskType, typename OtherCallbackType,
            typename... OtherArgTypes>
  friend class TaskImpl;

  template <size_t ArgIndex, typename ArgumentTask>
  void WithArg(const ArgumentTask& argument_task) {
    auto weak_this = std::enable_shared_from_this<
        TaskImpl<TaskType, CallbackType, ArgTypes...>>::weak_from_this();

    std::weak_ptr weak_argument(argument_task.impl_);

    argument_task.impl_->links_.emplace_back(
        [weak_this = std::move(weak_this),
         weak_argument = std::move(weak_argument)](Thread& thread) {
          const auto shared_this = weak_this.lock();
          const auto shared_argument = weak_argument.lock();
          if (shared_this && shared_argument) {
            std::lock_guard lock(shared_this->mutex_);
            assert(shared_argument->result_());
            std::get<ArgIndex>(shared_this->args_) = shared_argument->result_();
            if (--shared_this->pending_args_count_ == 0) {
              shared_this->Submit(thread);
              thread = Thread::kAny;
            }
          }
        });
  }

  std::mutex mutex_;
  TaskType task_;
  CallbackType callback_;
  ThreadPool thread_pool_;
  std::tuple<ArgTypes...> args_;
  size_t pending_args_count_;
  Result<std::invoke_result_t<TaskType, ArgTypes...>> result_;
  std::vector<std::function<void(Thread&)>> links_;
  std::atomic_flag is_completed_;
};
}  // namespace internal

/**
 * Represents a task in a task graph. `Task` instances are created when new
 * tasks are added to a task graph and represent the nodes of the graph.
 *
 * @see TaskGraph
 */
template <typename TaskType, typename CallbackType, typename... ArgTypes>
requires std::invocable<TaskType, ArgTypes...>
class Task {
 public:
  Task(const Task&) = default;
  Task(Task&&) = default;
  Task& operator=(const Task&) = default;
  Task& operator=(Task&&) = default;
  ~Task() = default;

  /**
   * Defines the arguments of the `Task` instance.
   *
   * @param argument_tasks `Task` instances the results of which must be taken
   * as the arguments in the same order.
   *
   * @return A reference to the same `Task` instance.
   */
  template <typename... ArgumentTasks>
  auto& WithArgs(ArgumentTasks&&... argument_tasks) {
    impl_->WithArgs(std::forward<ArgumentTasks>(argument_tasks)...);
    return *this;
  }

 private:
  friend class TaskGraph;

  template <typename OtherTaskType, typename OtherCallbackType,
            typename... OtherArgTypes>
  friend class internal::TaskImpl;

  explicit Task(
      std::shared_ptr<internal::TaskImpl<TaskType, CallbackType, ArgTypes...>>
          impl)
      : impl_(std::move(impl)) {}

  std::shared_ptr<internal::TaskImpl<TaskType, CallbackType, ArgTypes...>>
      impl_;
};

/**
 * A set of tasks and dependencies between them that can be represented as
 * a graph of tasks. Each task is a function that can take the results of one or
 * more other tasks as its arguments. The tasks are submitted for execution to
 * an instance of a thread pool.
 */
class TaskGraph {
 public:
  /**
   * Creates a `TaskGraph` instance.
   *
   * @param thread_pool A thread pool used to execute tasks.
   */
  explicit TaskGraph(ThreadPool thread_pool)
      : thread_pool_{std::move(thread_pool)} {}

  TaskGraph(const TaskGraph&) = default;
  TaskGraph(TaskGraph&&) = default;
  TaskGraph& operator=(const TaskGraph&) = default;
  TaskGraph& operator=(TaskGraph&&) = default;

  ~TaskGraph() {
    for (const auto& task : tasks_) {
      task->Wait();
    }
  }

  /**
   * Adds a new task to the `TaskGraph` instance.
   *
   * @param task The function to execute.
   * @param callback The callback that has to be called when the task is
   * completed.
   *
   * @return A `Task` instance that represents the new task.
   */
  template <typename... ArgTypes, typename TaskType,
            typename CallbackType = nullptr_t>
  requires std::invocable<TaskType, ArgTypes...>
  auto AddTask(TaskType&& task, CallbackType&& callback = {}) {
    using TaskImpl = internal::TaskImpl<TaskType, CallbackType, ArgTypes...>;

    auto task_impl = std::make_shared<TaskImpl>(
        std::forward<TaskType>(task), std::forward<CallbackType>(callback),
        thread_pool_);

    if (sizeof...(ArgTypes) == 0) {
      roots_.emplace_back([weak_task = std::weak_ptr<TaskImpl>(task_impl)] {
        if (const auto shared_task = weak_task.lock()) {
          shared_task->Submit(Thread::kAny);
        }
      });
    }

    tasks_.emplace_back(task_impl);
    return Task<TaskType, CallbackType, ArgTypes...>(task_impl);
  }

  /**
   * Begins submitting tasks for execution to the thread pool. Tasks that do not
   * have dependencies are submitted first. Other tasks are submitted when their
   * dependencies are completed.
   */
  void Submit() const {
    for (const auto& root : roots_) {
      root();
    }
  }

 private:
  ThreadPool thread_pool_;
  std::vector<std::function<void()>> roots_;
  std::vector<std::shared_ptr<internal::ITask>> tasks_;
};
}  // namespace scheduling
