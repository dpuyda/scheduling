# Contents

* [Introduction](#Introduction)
* [Usage](#Usage)
* [Examples](#Examples)
  * [Thread pool](#Thread-pool)
  * [Task graphs](#Task-graphs)
* [Building examples and tests](#Building-examples-and-tests)
* [License](#License)

# Introduction

`scheduling` is a simple and minimalistic header-only library that allows you
to:

* Submit tasks for execution to an instance of a thread pool.
* Parallelize execution of dependent tasks and task graphs.

`scheduling` requires C\++20.

# Usage

To use `scheduling`, simply add the content of
the [include](https://github.com/dpuyda/scheduling/tree/master/include)
folder to your project.

# Examples

The examples below show how to use `scheduling`.

## Thread pool

To use a thread pool, include [thread_pool.h](include/scheduling/thread_pool.h)
in your source code:

```cpp
#include "scheduling/thread_pool.h"
```

Next, create a `ThreadPool` instance and specify the maximum number of threads
that can be run simultaneously. For example:

```cpp
const auto max_threads_count = std::thread::hardware_concurrency();

scheduling::ThreadPool thread_pool(max_threads_count);
```

The examples below show how to use the `ThreadPool` instance.

1. To submit a function without parameters that returns `void`:

    ```cpp
    auto task = [] { std::this_thread::sleep_for(std::chrono::seconds(1)); };

    auto callback = [] { std::cout << "Completed!" << std::endl; };

    thread_pool.Submit(std::move(task), std::move(callback));
    ```

2. To submit a function without parameters that returns `int`:

    ```cpp
    auto task = [] { return 42; };

    auto callback = [](const int result) { std::cout << result << std::endl; };

    thread_pool.Submit(std::move(task), std::move(callback));
    ```

3. To submit a function that calculates the sum of two integers:

    ```cpp
    auto task = [](const int a, const int b) { return a + b; };

    auto callback = [](const int sum) { std::cout << sum << std::endl; };

    thread_pool.Submit(std::move(task), std::move(callback), 42, 53);
    ```

4. To submit a function with a given priority:

    ```cpp
    constexpr auto priority = scheduling::kDefaultPriority + 1;

    auto task = [] { return 42; };

    auto callback = [](const int result) { std::cout << result << std::endl; };

    thread_pool.Submit(priority, std::move(task), std::move(callback));
    ```

    > Note: The user code has to make sure that there is no thread starvation.
    > `ThreadPool` does not guarantee that a task will be executed when tasks of
    > higher priority are being submitted. A task with a higher priority is
    > always closer to the beginning of the task queue than a task with a lower
    > priority.

5. To cancel a task if it has not started yet:

    ```cpp
    const auto cancellation_token =
        thread_pool.Submit(std::move(task), std::move(callback));
    
    const auto is_cancelled = cancellation_token.Cancel();
    ```

    > Note: A task that has already started cannot be cancelled. The user code has
    > to make sure that such a task completes as soon as possible if needed.

## Task graphs

To use task graphs, include [task_graph.h](include/scheduling/task_graph.h) in your source code:

```cpp
#include "scheduling/task_graph.h"
```

The examples below show how to use task graphs.

### Calculate `a + b`

Assume that you want to calculate the sum of two integers `a` and `b`
asynchronously provided that getting the values of `a` and `b` and
the calculation take long.

![graph_1.png](images/graph_1.png)

To calculate `a + b`:

1. Create a `ThreadPool` instance and specify the maximum number of threads that
can be run simultaneously:

    ```cpp
    const auto max_threads_count = std::thread::hardware_concurrency();

    scheduling::ThreadPool thread_pool(max_threads_count);
    ```

2. Create a `TaskGraph` instance that uses a given thread pool:

    ```cpp
    scheduling::TaskGraph task_graph(thread_pool);
    ```

3. Add a task to get `a`:

    ```cpp
    const auto get_a = task_graph.AddTask([] {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        return 42;
    });
    ```

4. Add a task to get `b`:

    ```cpp
    const auto get_b = task_graph.AddTask([] {
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        return 53;
    });
    ```

5. Add a task to calculate `a + b`:

    ```cpp
    auto calculate_sum = [](const auto a, const auto b) {
      std::this_thread::sleep_for(std::chrono::milliseconds(300));
      return a + b;
    };

    auto sum_callback = [](const auto sum) { std::cout << sum << std::endl; };

    [[maybe_unused]] const auto get_result =
        task_graph
            .AddTask<int, int>(std::move(calculate_sum),
                               std::move(sum_callback))
            .WithArgs(get_a, get_b);
    ```

6. Submit the task graph for execution to the thread pool:

    ```cpp
    task_graph.Submit();
    ```

    Thread visualization can look as follows. On the image below, thread 18244
    executes `get_a` and thread 19088 executes `get_b` and `get_result`:

    ![threads_1_.png](images/threads_1.png)

### Calculate `(a + b) * (c + d)`

Assume that you want to calculate the value of `(a + b) * (c + d)`
asynchronously provided that getting the values of `a`, `b`, `c` and `d` and
the calculations take long.

![graph_2.png](images/graph_2.png)

To calculate `(a + b) * (c + d)`:

1. Create a `ThreadPool` instance and specify the maximum number of threads that
can be run simultaneously:

    ```cpp
    const auto max_threads_count = std::thread::hardware_concurrency();

    scheduling::ThreadPool thread_pool(max_threads_count);
    ```

2. Create a `TaskGraph` instance that uses a given thread pool:

    ```cpp
    scheduling::TaskGraph task_graph(thread_pool);
    ```

3. Add tasks to get the values of `a`, `b`, `c` and `d`:

    ```cpp
    const auto get_a = task_graph.AddTask([] {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        return 42;
    });

    const auto get_b = task_graph.AddTask([] {
        std::this_thread::sleep_for(std::chrono::milliseconds(125));
        return 53;
    });

    const auto get_c = task_graph.AddTask([] {
        std::this_thread::sleep_for(std::chrono::milliseconds(150));
        return 68;
    });

    const auto get_d = task_graph.AddTask([] {
        std::this_thread::sleep_for(std::chrono::milliseconds(175));
        return 94;
    });
    ```

4. Add tasks to calculate `a + b` and `c + d`:

    ```cpp
    const auto calculate_sum = [](const auto lhs, const auto rhs) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        return lhs + rhs;
    };

    const auto get_sum_1 =
        task_graph.AddTask<int, int>(calculate_sum).WithArgs(get_a, get_b);

    const auto get_sum_2 =
        task_graph.AddTask<int, int>(calculate_sum).WithArgs(get_c, get_d);
    ```

5. Add a task to calculate `(a + b) * (c + d)`:

    ```cpp
    auto calculate_product = [](const auto lhs, const auto rhs) {
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
      return lhs * rhs;
    };

    auto product_callback = [](const auto product) {
      std::cout << product << std::endl;
    };

    [[maybe_unused]] const auto get_result =
        task_graph
            .AddTask<int, int>(std::move(calculate_product),
                               std::move(product_callback))
            .WithArgs(get_sum_1, get_sum_2);
    ```

6. Submit the task graph for execution to the thread pool:

    ```cpp
    task_graph.Submit();
    ```

    Thread visualization can look as follows. On the image below, thread 17325
    executes `get_d`, `get_sum_2` and `get_result`, thread 12184 executes
    `get_a`, thread 8100 executes `get_b` and `get_sum_1` and thread 1744
    executes `get_c`:

    ![threads_2_.png](images/threads_2.png)

# Building examples and tests

To build examples and tests using the CMake script
[CMakeLists.txt](CMakeLists.txt):

1. Create a folder named "build" in the root of the git repository and navigate
to this folder:

    ```
    mkdir build && cd build
    ```

2. Run CMake from this folder:

    ```
    cmake ..
    ```

    Use the `BUILD_EXAMPLES` and `BUILD_TESTS` options to define what targets
    to build. The default value of both options is `ON`.

# License

`scheduling` is licensed under the [MIT License](LICENSE).
